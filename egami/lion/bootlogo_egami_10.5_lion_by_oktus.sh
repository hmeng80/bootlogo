#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "**  https://www.tunisia-sat.com/forums/threads/3745208/   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/bootlogo.tar.gz "https://gitlab.com/hmeng80/bootlogo/-/raw/main/egami/lion/bootlogo.tar.gz?inline=false"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/bootlogo.tar.gz

sleep 2s
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2s
init 3
exit 0
