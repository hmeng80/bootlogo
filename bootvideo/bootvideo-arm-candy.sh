#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/bootvideo-candy-mod-haitham_1.0-arm_all.ipk "https://gitlab.com/hmeng80/bootlogo/-/raw/main/bootvideo/bootvideo-candy-mod-haitham_1.0-arm_all.ipk"
opkg install --force-reinstall --force-overwrite /tmp/*.ipk

rm -r /tmp/bootvideo-candy-mod-haitham_1.0-arm_all.ipk

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ REBOOT ENIGMA... #################" 

sleep 1
exit 0
