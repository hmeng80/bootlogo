#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /usr/share/oatv-bootlogo/bootvideo.mp4 "https://gitlab.com/hmeng80/bootlogo/-/raw/main/bootvideo/christmas-fireplace.mp4"

echo ""
cd ..
sync
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
 
sleep 2s

exit 0
